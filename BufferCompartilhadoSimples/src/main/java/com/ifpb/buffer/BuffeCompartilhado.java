package com.ifpb.buffer;

/**
 * @author raul
 * @project 01
 */
public class BuffeCompartilhado implements Buffer {

    // Inicia-se com -1 pra "garantir" que a execução das threads seja simultânea
    private Integer buffer = -1;

    // Metodo SET Sobrecrito da interface Buffer. Será utilizado pelo produtor para atribuir um valor ao valor final.
    public void set(int valor) {
        System.out.println("Produtor seta " + valor);
        this.buffer = valor;
    }

    // Metodo GET Sobrecrito da interface Buffer. Será utilizado pelo consumidor para recuperar o valor final
    public int get() {
        return buffer;
    }
}
