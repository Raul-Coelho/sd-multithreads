package com.ifpb.main;

import com.ifpb.buffer.Buffer;
import com.ifpb.buffer.BuffeCompartilhado;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author raul
 * @project 01
 */
public class Main {
    public static void main(String[] args) {
        // Cria-se duas threads pra executar o Producer o Consumer
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        // Compartilhamento de buffer entre produtor e consumidor
        Buffer buffer = new BuffeCompartilhado();

        // Executando o Producer e passando o buffer e os argumentos como parâmetro
        executorService.execute(new Produtor(buffer));
        // Executando o Consumer e passando o buffer e a quantidade de argumentos como parâmetro
        executorService.execute(new Consumidor(buffer));

        // Stop nas threads
        executorService.shutdown();
    }
}
