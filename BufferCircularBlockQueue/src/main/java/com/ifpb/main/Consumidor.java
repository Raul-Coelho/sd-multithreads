package com.ifpb.main;

import com.ifpb.buffer.Buffer;

import java.util.Random;

public class Consumidor implements Runnable {

    private static Random gerador = new Random();
    private Buffer bufferCompartilhado;

    public Consumidor(Buffer compartilhado) {
        bufferCompartilhado = compartilhado;
    }

    public void run() {
        // Usado pra adicionar os novos valores
        int valorFinal = 0;

        // Quantidade de iteração será igual ao valor definido no artigo
        for (int cont = 1; cont <= 10; cont++) {
            try {

                valorFinal += bufferCompartilhado.get();
                // Fazendo a thread dormir de 0 a 4000 milisegundos
                Thread.sleep(gerador.nextInt(4000));
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
        System.out.println("Fim do Consumidor, Valor da soma: " + valorFinal);
    }
}
