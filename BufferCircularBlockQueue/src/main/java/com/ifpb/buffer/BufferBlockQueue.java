package com.ifpb.buffer;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BufferBlockQueue implements Buffer {

    // Implementa a estrutura de buffer circular e cuida de toda a sincronização
    private final BlockingQueue<Integer> queue;

    public BufferBlockQueue() {
        this.queue = new ArrayBlockingQueue<>(3);
    }


    // Implementação do método ser da interface Buffer. Será utilizado pelo produtor para atribuir um valor
    @Override
    public void set(int valor) {
        try {
//            Adicionando novo valor
            queue.put(valor);
            System.out.println("Produtor grava " + valor);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    // Implementação do método get da interface Buffer. Será utilizado pelo consumidor
    @Override
    public int get() {
        int valueRead = 0;
        try {
//            pegando valor
            valueRead = queue.take();
            System.out.println("Consumidor le " + valueRead);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        return valueRead;
    }
}
