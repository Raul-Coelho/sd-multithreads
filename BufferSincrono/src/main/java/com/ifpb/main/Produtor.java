package com.ifpb.main;

import com.ifpb.buffer.Buffer;

import java.util.Random;

/**
 * @author raul
 * @project 01
 */
public class Produtor implements Runnable {

    // Aleatoriza um valor
    private static Random gerador = new Random();
    // Buffer compartilhado entre o Producer e o Consumer
    private Buffer localizacaoCompartilhada;

    public Produtor(Buffer compartilhado) {
        localizacaoCompartilhada = compartilhado;
    }

    public void run() {
        for (int cont = 1; cont <= 10; cont++) {
            try {
                // Atribui valor ao buffer
                localizacaoCompartilhada.set(cont);
                // Thread dorme de 0 a 4000 milisegundos
                Thread.sleep(gerador.nextInt(4000));
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
        System.out.printf("Produtor Conclui!");
    }
}
