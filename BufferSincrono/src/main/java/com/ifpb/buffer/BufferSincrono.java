package com.ifpb.buffer;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BufferSincrono implements Buffer {

    // Uma unica Thread acessa os recursos
    private Lock lock = new ReentrantLock(false);
    // Coloca a thread do Produtor em estado de espera e retirala desse mesmo estado
    private Condition podeLer = lock.newCondition();
    // Coloca a thread do Consumidor em estado de espera e retirala desse mesmo estado
    private Condition podeEscrever = lock.newCondition();

    private Integer buffer = 0;
//    Informa se o buffer está vazio ou não
    private Boolean empty = true;

    // Set, utilizado pelo produtor para adicionar os novos valores
    @Override
    public void set(int valor) {
        // Quando a thread do produtor chegar nesse ponto, o Consumidor não poderar ter acesso aos recursos até que o lock seja liberado
        lock.lock();

        try {
            // Produtor verifica se o buffer está cheio, e aguarda ate que seja lido
            while (!empty) {
                System.out.println("Buffer Cheio, Aguarde");
                // Produtor fica esperando até que a o Consimudor mude a condição
                podeEscrever.await();
            }

            buffer = valor;
            // Buffer cheio, e o valor fica como false pois o valor foi lido pela thread do Consumidor
            empty = false;
            System.out.println("Produtor grava " + buffer);
            // Retira o Produtor do Estado de Espera
            podeLer.signal();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }

    }

    // Get, utilizado pelo consumidor para recuperar os valores
    @Override
    public int get() {
//      lock so e liberado quando a thread do consumidor chega a esse ponto, ate la, o produtor nao pode ter acesso
        lock.lock();

        try {
            // Se o consumidor tiver o lock enquanto não existe nenhum valor no buffer ele entrará na condicional
            while (empty) {
                System.out.println("Consumidor tenta consumir, mas o buffer esta vazio");
                // Consumidor fica esperando até que a o Produtor mude a condição
                podeLer.await();
            }

            empty = true;
            // Buffer vazio, e o valor fica como true pois o valor foi lido pela thread do Consumidor
            System.out.println("Consumidor le : " + buffer);
            // Retira o Produtor do Estado de Espera
            podeEscrever.signal();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            // Para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }

        return buffer;
    }
}
