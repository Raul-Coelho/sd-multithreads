package com.ifpb.buffer;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BufferCircular implements Buffer {

    // Apenas uma thread acessa os recursos
    private Lock lock = new ReentrantLock(false);

    private Condition canRead = lock.newCondition();
    private Condition canWrite = lock.newCondition();

    // Array onde será guardado os valores do Produtor
    private Integer[] buffer = {-1, -1, -1};
    // Posições ocupadas do Buffer
    private Integer usedBuffers = 0;
    // Indica a posição que deve ser lida pelo buffer
    private Integer readIndex = 0;
    // Indica a posição que deve ser escrita
    private Integer writeIndex = 0;

    @Override
    public int get() {
        Integer valueRead = 0;
//      lock so e liberado quando a thread do consumidor chega a esse ponto, ate la, o produtor nao pode ter acesso
        lock.lock();

        try {
            // Se o consumidor tiver o lock enquanto não existe nenhum valor no buffer ele entrará na condicional
            if (usedBuffers.equals(0)) {

                System.out.println("Consumidor tenta ler, mas o Buffer esta vazio. Consumidor espera.");
                // Consumidor fica esperando até que a o Produtor mude a condição
                canRead.await();
            }
            // Lendo Valores
            valueRead = buffer[readIndex];

            System.out.println("Consumidor le " + valueRead + " posicao " + readIndex);

            // Redefinindo o indice de leitura
            if (readIndex == (buffer.length - 1)) {
                readIndex = -1;
            }

            // Incrementado o indice de valores lidos
            readIndex++;
            // Decrementando em 1 a variável que indica a quantidade de buffers ocupados
            usedBuffers--;
            // a thread do producer e removida do estado de espera
            canWrite.signal();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // Para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }

        return valueRead;
    }

    // Set, utilizado pelo produtor para adicionar os novos valores
    @Override
    public void set(int value) {

        try {
            // Quando a thread do produtor chegar nesse ponto, o Consumidor não poderar ter acesso aos recursos até que o lock seja liberado
            lock.lock();
            // Se o consumidor tiver o lock enquanto não existe nenhum valor no buffer ele entrará na condicional
            if (usedBuffers.equals(buffer.length)) {
                System.out.println("Produtor tenta gravar, mas o Buffet esta cheio. Produtor espera.");
                // A thread do produtor será posta em estado de espera até que a condição seja mudada pela thread do Consumidor
                canWrite.await();
            }

            //Escrevendo no buffer
            buffer[writeIndex] = value;

            System.out.println("Produtor escreve " + value + " posicao " + writeIndex);

            // Redefinindo o indice de leitura
            if (writeIndex == (buffer.length - 1)) {
                writeIndex = -1;
            }

            // Incrementado o indice de valores lidos
            writeIndex++;
            // Decrementando em 1 a variável que indica a quantidade de buffers ocupados
            usedBuffers++;

            // Retira o Produtor do Estado de Espera
            canRead.signal();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // Para casos de impasse onde os bloqueios nunca são liberados
            lock.unlock();
        }

    }


}
