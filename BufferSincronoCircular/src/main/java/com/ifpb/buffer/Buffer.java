package com.ifpb.buffer;

/**
 * @author raul
 * @project 01
 */
public interface Buffer {
    public void set(int valor);
    public int get();
}
